# KPF Qt

## Requirements - Win32
Qt Creator. (Qt Framework 5.4)  
MinGW 32 bit  
GnuWin32 (make for windows)

## Requirements - Linux
Qt 5 Framework. - Required to compile & run

### Qt5 install Arch
$ sudo pacman -S qt5-base qt5-webkit

### Qt5 install other linux
[Qt Creator (Open Source Edition)](http://www.qt.io/download/)

## Running
Windows is easy. Required dlls are packaged with the application. Just run kpf-qt.exe

Linux requires installing the Qt framework. v5.4+ is required. Just use the link above, or run the command under Arch if you're an Arch user.

